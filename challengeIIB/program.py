# author: jfox13
import sys
import heapq

def makeList(grid,rows,columns,numToRow):
    ''' takes in grid, makes adjacency list '''
    numNode = rows * columns
    adjlist = []
    for _ in range(numNode):
        adjlist.append(list())
        #adjList.append(list())
    adjlist.append(list())
    for r in range(rows):
        for c in range(columns):
            pos = r * columns + c
            numToRow[pos] = r
    for r in range(rows):
        for c in range(columns - 1):
            pos = r * columns + c
            '''
            # none of this is needed you can only move right to left
            if not c == 0: # if not first column
                adjlist[pos].append( (pos-1,grid[r][c-1],c-1) ) # L if not left edge
                if r != 0: #
                    adjlist[pos].append( (pos-1-columns,grid[r-1][c-1],c-1) ) # LU if not top
                else:
                    adjlist[pos].append( ( (rows-1)*columns+c-1,grid[rows-1][c-1],c-1) ) # LU if top
                if r != rows - 1:     
                    adjlist[pos].append( (pos-1+columns,grid[r+1][c-1],c-1) )    # LD if not bottom
                else:
                    adjlist[pos].append( (c-1,grid[0][c-1],c-1) )    # LD if bottom
            '''
            
            ''' dont need this if statement '''
            if not c == columns -1: # if not last column
                adjlist[pos].append( (pos+1,grid[r][c+1],c+1) ) # R if not left edge
                if r != 0:
                    adjlist[pos].append( (pos+1-columns,grid[r-1][c+1],c+1) ) # RU if not top
                else:
                    adjlist[pos].append( ( (rows-1)*columns+c+1,grid[rows-1][c+1],c+1) ) # RU if top
                if r != rows - 1:
                    adjlist[pos].append( (pos+1+columns,grid[r+1][c+1],c+1) )    # RD if not bottom
                else:
                    adjlist[pos].append( (c+1,grid[0][c+1],c+1) )    # RD if bottom
            '''
            # also not needed bcs only left to right
            if r != 0:
                adjlist[pos].append( (pos-columns,grid[r-1][c],c) ) # U if not top
            else:
                adjlist[pos].append( ( (rows-1)*columns+c,grid[rows-1][c],c) ) # U if top

            if r != rows - 1:
                adjlist[pos].append( (pos+columns,grid[r+1][c],c) )    # D if not bottom
            else:
                adjlist[pos].append( (c,grid[0][c],c) )    # D if bottom
            '''
    return adjlist
    

def search(adjList, start, columns, startWeight,r):
    frontier = []
    visited = {}
    best = []
    bestLen = float('inf')

    heapq.heappush(frontier,(startWeight,start,start,0))
    #print("go")
    done = False
    while frontier and not done:
        #print(frontier)
        weight, source, target, c = heapq.heappop(frontier)
        #for i in ends: # and not done
        #print("weight",weight)
        if c == columns -1:
            if weight <= bestLen:
                pathStr = printThrough(visited,source,target)
                replace = False
                for num in range(1,min(len(best),len(pathStr))):
                    if best[-num] < pathStr[-num]:
                        break
                    elif best[-num] > pathStr[-num]:
                        replace = True
                        break
                if (replace and weight == bestLen) or weight < bestLen:
                    #if replace:
                    #print(pathStr,best)
                    #print("HELLO")
                    best = pathStr
                    #print(bestLen,weight)
                    bestLen = weight
                    #print("HERE",bestLen)
                #done = True
        if target in visited and r < 3:
            continue
        visited[target] = source
        for neighbor, cost, c in adjList[target]:
            heapq.heappush(frontier, (weight + cost,target,neighbor,c))
    if best:
        #print("Cost: {} Path: {}".format(weight,pathStr))
        #print("weight",weight)
        #return weight, pathStr
        return bestLen, best
    else:
        return 0, None
        #print("Cost: 0 Path: None")

def printThrough(visited,source,target):
    path = []
    path.append(str(target))
    while True:
        path.append(str(source))
        source = visited[source]
        if visited[source] == source:
            path.append(str(source))
            break
    path = path[::-1]
    #pathStr = " ".join(path)
    return path

if __name__ == '__main__':
    ''' parses inputs '''
    inputs = list(sys.stdin)
    #for i in inputs:
    #    print(i)
    run = 0
    while len(inputs) > 0 :
        first = inputs[0].strip().split()
        rows = int(first[0])
        columns = int(first[1])
        if rows == 0 and columns == 0:
            break
        inputs = inputs[1:]
        grid = []
        start = -1
        ends = []
        for i in range(rows):
            newRow = inputs[i].strip().split()
            newRow = [ int(j) for j in newRow ]
            grid.append( newRow )
            '''
            for index, val in enumerate(newRow):
                if val == 'S':
                    start = i * columns + index
                if val == 'E':
                    ends.append(i * columns + index)
            '''
        #for i in grid:
        #    print(i)
        #print()
        numToRow = {}
        adjList = makeList(grid,rows,columns,numToRow)
        minCost = float('inf')
        path = []
        #w, p = search(adjList, 5*columns, columns, grid[5][0])
        #x = [ str(1+numToRow[int(j)]) for j in p ]
        #print(w, p)
        #print(w, x)
        #print("AHHHHHHHHH")
        if columns != 1:
            for i in range(rows):
                #print(i)
                w, p = search(adjList, i*columns, columns, grid[i][0],run)
                x = [ str(1+numToRow[int(j)]) for j in p ]
                #print(w, p)
                #print(w, x)
                if min(minCost,w) != minCost:
                    minCost = w
                    if len(p) > 2 and p[0] == p[1]:
                        p = p[1:]
                    #print(p,w)
                    path = [ str(1+numToRow[int(j)]) for j in p ]
        else:
            for i in range(rows):
                w = grid[i][0]
                if min(minCost,w) != minCost:
                    minCost = w
                    path = [ str(i+1) ]
        print(minCost)
        print(' '.join(path))
        
        #print()
        #print()
        #for i in adjList:
        #    print(i)
        #search(adjList,start,ends)
        inputs = inputs[rows:]
        run += 1
