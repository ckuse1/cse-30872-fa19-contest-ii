#!/usr/bin/env python3

import sys
import math

graph = {}
longset = set()
foundlist = []

def find_longest(start, myset, found):
	longest = 0
	correct = set()
	found2 = found.copy()
	foundlist.append(found2)
	if start in graph.keys():
		for key in graph[start]:
			if (start, key) in myset:
				continue		# edge already seen
			else:
				found.append(key)
				myset.add((start, key))
				myanswer = find_longest(key, myset, found)
				if len(myanswer) > longest:
					longest = len(myanswer)
					found = myanswer
				myset.remove((start, key))
				found.remove(key)
	
	return found

def ismorph(word1, word2):
	word1 = list(word1)
	word2 = list(word2)
	if len(word1) < len(word2):
		mymin = len(word1)		# word 1 is smaller word
	else:
		mymin = len(word2)
		wbuffer = word2
		word2 = word1
		word1 = wbuffer

	if len(word1) == len(word2):		# lengths are same
		numdiff = 0
		for i in range(0, mymin):
			if word1[i] != word2[i]:
				numdiff+= 1
		if numdiff > 1:
			return False
		else:
			return True

	if abs(len(word1) - len(word2)) > 1:
		return False

	if word2[1:] == word1 or word1[1:] == word2:
		return True
	off = 0
	j = 0
	for i in range(0, mymin):			# lengths are diff
		if word1[i] != word2[j]:
			off += 1
			j += 2
			if off > 1:
				return False
		else:
			j+=1
	if word2[mymin] not in word1 and off == 1:
		return False

	return True

def make_graph(wdict):
	
	for i in range(0, len(wdict)):
		for j in range(i, len(wdict)): 
			if i == j:
				continue
			if ismorph(wdict[i], wdict[j]): 
				if wdict[i] in graph.keys():
					if wdict[j] not in wdict[i]:
						graph[wdict[i]].append(wdict[j])
				else:
					mylist = []
					mylist.append(wdict[j])
					graph[wdict[i]] = mylist
	
	for k in graph.keys():
		temp = []
		for l in graph[k]:
			if l not in temp:
				temp.append(l)
		graph[k] = temp

if __name__ == '__main__':
	dictionary = []
	count = 0
	for line in sys.stdin:
		dictionary.append(line.strip())
		count +=1 

	make_graph(dictionary)

	longest = []
	for line in graph.keys():
		templist = []
		templist.append(line)
		newlongest = find_longest(line, set(), templist)
		longest.append(newlongest)


	temp = set()
	for k in range(0, len(longest)):
		newlong = longest[k]
		if len(newlong) > len(temp):
			temp = newlong

	largest = []
	for i in range(0, len(foundlist)):
		if len(foundlist[i]) > len(largest):
			largest = foundlist[i]
			
	print(len(largest))
	for j in range(0, len(largest)):
		print(largest[j])

